class ResumeController < ApplicationController
  def index
    @user = User.find_by(email: 'beanjarahoussen@gmail.com')
    @url = 'resume'
  end

  def download
    send_file(
      "#{Rails.root}/public/beanjara.pdf",
      filename: "beanjara.pdf",
      type: "application/pdf"
    )
  end
end
